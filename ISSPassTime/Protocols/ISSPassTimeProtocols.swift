//
//  ISSPassTimeProtocols.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol ISSPassTimesWireFrameProtocol: class {
    static func createISSPassTimesModule() -> UIViewController
}

//MARK: VIEW -> PRESENTER
protocol ISSPassTimesPresenterProtocol: class {
    var view: ISSPassTimesViewProtocol? { get set }
    var interactor: ISSPassTimesInteractorInputProtocol? { get set }
    var wireFrame: ISSPassTimesWireFrameProtocol? { get set }
 
    func viewDidLoad()
    func didRetrieveISSPassTimes(_ passTimes: ISSPassTimeDataModel, currentAddress: String)
    func didRetrieveISSCurrentLocationData(_ currentLocationData: ISSNowDataModel)
    func onError(error:NSError)
}

//MARK: PRESENTER -> VIEW
protocol ISSPassTimesViewProtocol: class {
    var presenter: ISSPassTimesPresenterProtocol? { get set }
    func showISSPassTimes(with passTimes: ISSPassTimeDataModel, currentAddress: String)
    func showISSCurrentLocation(with currentLocationData: ISSNowDataModel)
    func showError(error:NSError)
}

//MARK: PRESENTER -> INTERACTOR
protocol ISSPassTimesInteractorInputProtocol: class {
    var presenter: ISSPassTimesPresenterProtocol? { get set }
    var localDatamanager: ISSPassTimesLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: ISSPassTimesRemoteDataManagerInputProtocol? { get set }
    func retrieveISSPassTimesList()
    func retrieveISSCurrentLocation()
}

//MARK: INTERACTOR -> PRESENTER
protocol ISSPassTimesInteractorOutputProtocol: class {
    func didRetrieveISSPassTimes(_ passTimes: ISSPassTimeDataModel, currentAddress: String)
    func didRetrieveISSCurrentLocationData(_ currentLocationData: ISSNowDataModel)
    func onError(error:NSError)
}

//MARK: INTERACTOR -> DATAMANAGER
protocol ISSPassTimesLocalDataManagerInputProtocol: class {
    
}

//MARK: LOCALDATAMANAGER -> INTERACTOR
protocol ISSPassTimesLocalDataManagerOutputProtocol: class {
    func onPassTimesRetrieved(_ passTimes: ISSPassTimeDataModel, currentAddress: String)
    func onISSCurrentLocationDataRetrieved(_ currentLocationData: ISSNowDataModel)
}

//MARK: INTERACTOR -> REMOTEDATAMANAGER
protocol ISSPassTimesRemoteDataManagerInputProtocol: class {
    var remoteRequestHandler: ISSPassTimesRemoteDataManagerOutputProtocol? { get set }
    func retrieveISSPassTimesList()
    func retrieveISSCurrentLocation()
}

//MARK: REMOTEDATAMANAGER -> INTERACTOR
protocol ISSPassTimesRemoteDataManagerOutputProtocol: class {
    func onPassTimesRetrieved(_ passTimes: ISSPassTimeDataModel, currentAddress: String)
    func onISSCurrentLocationDataRetrieved(_ currentLocationData: ISSNowDataModel)
    func onError(error:NSError)
}

