//
//  NetworkRouter.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import Alamofire

struct AlamofireRequestRouter:ISSNetworkRouter {
    
    func makeRequest(url: String, resource: RequestResource?, completionHandler: @escaping (Outcome<AnyObject>) -> ()) {
        print(url)
        Alamofire.request(url).responseData { (resData) -> Void in
            print(resData)
            if (resData.response != nil){
                if (resData.response?.statusCode == 200){
                    NSLog(NSString(data: resData.data!, encoding: String.Encoding.utf8.rawValue)! as String)
                    switch resData.result{
                    case .success(let data as AnyObject):
                        completionHandler(Outcome.Value(data))
                    case .failure(let data as AnyObject):
                        completionHandler(Outcome.Value(data))
                    }
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Server Error"])
                    completionHandler(Outcome.Error(error, nil))
                }
            } else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "No internet Connection"])
                completionHandler(Outcome.Error(error, nil))
            }
        }
        }
    }
    

