//
//  ISSPassTimeDataModel.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import UIKit

public struct ISSPassTimeDataModel: Decodable {
    let response:[PassTimesData]
    let message: String
}

public struct PassTimesData: Decodable {
    let duration: Int
    let risetime: Int
}

