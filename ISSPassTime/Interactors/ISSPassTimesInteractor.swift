//
//  ISSPassTimesInteractor.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

class ISSPassTimesInteractor: ISSPassTimesInteractorInputProtocol {
    weak var presenter: ISSPassTimesPresenterProtocol?
    var localDatamanager: ISSPassTimesLocalDataManagerInputProtocol?
    var remoteDatamanager: ISSPassTimesRemoteDataManagerInputProtocol?
    
    func retrieveISSCurrentLocation() {
        remoteDatamanager?.retrieveISSCurrentLocation()
    }
    
    func retrieveISSPassTimesList() {
        remoteDatamanager?.retrieveISSPassTimesList()
    }
    
}

extension ISSPassTimesInteractor: ISSPassTimesRemoteDataManagerOutputProtocol {
    func onError(error: NSError) {
        presenter?.onError(error: error)
    }
    
    func onISSCurrentLocationDataRetrieved(_ currentLocationData: ISSNowDataModel) {
        presenter?.didRetrieveISSCurrentLocationData(currentLocationData)
    }
    
    func onPassTimesRetrieved(_ passTimes: ISSPassTimeDataModel, currentAddress: String) {
        presenter?.didRetrieveISSPassTimes(passTimes, currentAddress: currentAddress)
    }
    
}
