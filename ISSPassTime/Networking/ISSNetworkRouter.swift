//
//  NetworkInterface.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

protocol ISSNetworkRouter{
    func makeRequest(url:String, resource:RequestResource?, completionHandler: @escaping (Outcome<AnyObject>) -> ())
}

enum RequestMethod:String{
    case POST = "POST"
    case GET = "GET"
}

struct RequestResource {
    let endpoint: NSURL
    let method:RequestMethod
    var requestBody:NSData?
    var headers = [String:String]()
    
    init(endpoint:NSURL, method:RequestMethod, body:NSData?, headers:[String:String]){
        self.endpoint = endpoint
        self.method = method
        self.requestBody = body
        self.headers = headers
    }
    
}
enum Outcome<T>{
    case Value(T)
    case Error(NSError, AnyObject?)
}


