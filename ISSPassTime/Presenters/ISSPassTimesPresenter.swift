//
//  ISSPassTimesPresenter.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

class ISSPassTimesPresenter: ISSPassTimesPresenterProtocol {
    
    weak var view: ISSPassTimesViewProtocol?
    var interactor: ISSPassTimesInteractorInputProtocol?
    var wireFrame: ISSPassTimesWireFrameProtocol?
    
    func viewDidLoad() {
        interactor?.retrieveISSPassTimesList()
        interactor?.retrieveISSCurrentLocation()
    }
    
}

extension ISSPassTimesPresenter: ISSPassTimesInteractorOutputProtocol {
    
    func onError(error:NSError) {
         view?.showError(error: error)
    }
    
    func didRetrieveISSPassTimes(_ passTimes: ISSPassTimeDataModel, currentAddress: String) {
        view?.showISSPassTimes(with: passTimes, currentAddress: currentAddress)
    }
    
    func didRetrieveISSCurrentLocationData(_ currentLocationData: ISSNowDataModel) {
        view?.showISSCurrentLocation(with: currentLocationData)
    }
}
