//
//  ISSPassTimeTests.swift
//  ISSPassTimeTests
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import XCTest
import CoreLocation
@testable import ISSPassTime

class ISSPassTimeTests: XCTestCase {
    
    var networkService:ISSService! = nil
    
    override func setUp() {
        super.setUp()
        networkService = ISSService()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPassTimeAPI() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        let location = CLLocation(latitude: CLLocationDegrees("45.0")!, longitude: CLLocationDegrees("122.3")!)
        networkService.getPassTime(forLocation: location, completionHandler: { (outcome) in
            switch outcome {
            case .Value(let data):
                XCTAssertNotNil(data, "No data was downloaded.")
                ex.fulfill()
            case .Error( let e, _):
                XCTFail("error: \(e)")
            }
        })
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func testISSNowAPI() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        networkService.getISSLocation { (outcome) in
            switch outcome {
            case .Value(let data):
                XCTAssertNotNil(data, "No data was downloaded.")
                ex.fulfill()
            case .Error( let e, _):
                XCTFail("error: \(e)")
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
}
