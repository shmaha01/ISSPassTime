//
//  ISSService.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import MapKit

private let ISSRootURL = "http://api.open-notify.org/"

struct ISSService {
    
    let networkRouter:ISSNetworkRouter
    
    init() {
        self.networkRouter = AlamofireRequestRouter()
    }
    
    func getISSLocation(completionHandler: @escaping (Outcome<AnyObject>)->Void) {
        /* Fetch ISSLocation */
        let url = ISSRootURL+"iss-now.json"
        self.networkRouter.makeRequest(url: url, resource: nil, completionHandler:{
            (outcome) -> Void in
            switch outcome{
            case .Value(let v):
                do{
                    let nowDataModel = try ISSNowDataModel.decode(data: v as! Data)
                    completionHandler(Outcome.Value(nowDataModel as AnyObject))
                }
                catch {
                    print(error.localizedDescription)
                }
            case .Error(let e, _):
                print (e.debugDescription)
            }
            
        })
    }
    
    func getPassTime(forLocation location: CLLocation, completionHandler: @escaping (Outcome<AnyObject>)->Void) {
        /* Fetch PassTime */
        let lat:String = String(format:"%f", location.coordinate.latitude)
        let long:String = String(format:"%f", location.coordinate.longitude)
        let url = ISSRootURL+"iss-pass.json?lat="+lat+"&lon="+long+"&alt=20&n=10"
        self.networkRouter.makeRequest(url: url, resource: nil, completionHandler:{
            (outcome) -> Void in
            switch outcome{
            case .Value(let v):
                do{
                    let passDataModel = try ISSPassTimeDataModel.decode(data: v as! Data)
                    completionHandler(Outcome.Value(passDataModel as AnyObject))
                }
                catch {
                    
                }       
            case .Error(let e, _):
                print (e.debugDescription)
                completionHandler(Outcome.Error(e, nil))
            }
        })
    }
    
}
