//
//  ISSNowDataModel.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import UIKit

public struct ISSNowDataModel: Decodable {
    let iss_position:ISSCurrentLocation
    let message: String
}

public struct ISSCurrentLocation: Decodable {
    let latitude: String
    let longitude: String
}


