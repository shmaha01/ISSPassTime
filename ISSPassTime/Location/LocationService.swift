//
//  LocationService.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

import UIKit
import CoreLocation

public class LocationService: NSObject, CLLocationManagerDelegate{
    
    public static var sharedInstance = LocationService()
    let locationManager: CLLocationManager
    var currentLocation: CLLocation = CLLocation.init()
    var locationCompletionHandler:((Outcome<AnyObject>)->Void)?
    
    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        super.init()
        locationManager.delegate = self
    }
    
    func getRecentLocation() -> CLLocation {
        return self.currentLocation
    }
    
    
    func startUpdatingLocation(completionHandler: @escaping (Outcome<AnyObject>)->Void ){
        self.locationCompletionHandler = completionHandler
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }else{
            //tell view controllers to show an alert
            showTurnOnLocationServiceAlert()
        }
    }
    
    
    //MARK: CLLocationManagerDelegate protocol methods
    public func locationManager(_ manager: CLLocationManager,
                                didUpdateLocations locations: [CLLocation]){
        if let newLocation = locations.last{
            print("(\(newLocation.coordinate.latitude), \(newLocation.coordinate.latitude))")
            self.currentLocation = newLocation
            if let handler = self.locationCompletionHandler {
                handler(Outcome.Value(newLocation as AnyObject))
            }
            
        }
    }
    
    
    public func locationManager(_ manager: CLLocationManager,
                                didFailWithError error: Error){
        if (error as NSError).domain == kCLErrorDomain && (error as NSError).code == CLError.Code.denied.rawValue{
            //User denied your app access to location information.
            showTurnOnLocationServiceAlert()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didChangeAuthorization status: CLAuthorizationStatus){
        if status == .authorizedWhenInUse{
            //You can resume logging by calling startUpdatingLocation here
        }
    }
    
    func showTurnOnLocationServiceAlert(){
        NotificationCenter.default.post(name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
    
    func notifiyDidUpdateLocation(newLocation:CLLocation){
        NotificationCenter.default.post(name: Notification.Name(rawValue:"didUpdateLocation"), object: nil, userInfo: ["location" : newLocation])
    }
    
    func getReverseGeocodeLocation(location:CLLocation, completion: @escaping (Outcome<AnyObject>) -> Void) {
        let geoCoder = CLGeocoder()
        var reverselocation:String?
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let address = placemarks?[0] else {
                return
            }
            reverselocation = (address.name ?? "could not reverse geocode the coordinates")+", "+(address.locality ?? "unknown")
            print("geocode"+reverselocation!)
            completion(Outcome.Value(reverselocation as AnyObject))
        })
    }

}
