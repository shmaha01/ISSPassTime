//
//  ISSPassTimeTableViewCell.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 17/02/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class ISSPassTimeTableViewCell: UITableViewCell {

    @IBOutlet var dateLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func convertTimeStamptoDate(timestamp:Int) -> String{
        let date = NSDate(timeIntervalSince1970: TimeInterval(timestamp))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        return dayTimePeriodFormatter.string(from: date as Date)
    }

}
