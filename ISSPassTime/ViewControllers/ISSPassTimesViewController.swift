//
//  ISSPassTimesViewController.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import MapKit

class ISSPassTimesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView:MKMapView!
    
    var presenter: ISSPassTimesPresenterProtocol?
    var passTimesData: ISSPassTimeDataModel?
    var passTimesArray: [PassTimesData] = []
    var userCurrentAddress:String = "trying to reverse geocode your coordinate"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        self.navigationItem.title = "International Space Station"
        NotificationCenter.default.addObserver(self, selector: #selector(ISSPassTimesViewController.showTurnOnLocationServiceAlert(_:)), name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ISSPassTimesViewController: ISSPassTimesViewProtocol {
    
    func showISSPassTimes(with passTimes: ISSPassTimeDataModel, currentAddress: String) {
        passTimesData = passTimes
        passTimesArray = passTimes.response
        self.userCurrentAddress =  currentAddress
        tableView.reloadData()
    }
    
    func showISSCurrentLocation(with currentLocationData: ISSNowDataModel) {
        let currentISSLocation = currentLocationData.iss_position
        DispatchQueue.main.async {
            let coord:CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(currentISSLocation.latitude)!, CLLocationDegrees(currentISSLocation.longitude)!)
            let span:MKCoordinateSpan  = MKCoordinateSpanMake(100, 180);
            let viewRegion = MKCoordinateRegionMake(coord, span);
            self.mapView.setRegion(viewRegion, animated: true)
            let annotation = ISSCustomPointAnnotation()
            annotation.pinCustomImageName = "ISSPin"
            annotation.coordinate = coord
            annotation.title = "ISS"
            annotation.subtitle = "International Space Station"
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            self.mapView.addAnnotation(pinAnnotationView.annotation!)
        }
    }
    
    func showError(error:NSError) {
        showUIAlert(title: error.localizedDescription, message: "Loading Pass Times from the local JSON file", leftAction: nil, rightAction: nil, controller: self)
    }
    
}

extension ISSPassTimesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "passtimecell", for: indexPath) as! ISSPassTimeTableViewCell
        //if(self.passTimesArray != nil){
            cell.dateLabel.text = cell.convertTimeStamptoDate(timestamp: (self.passTimesArray[indexPath.row].risetime))
        //}
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return passTimesArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var cell =  tableView.dequeueReusableCell(withIdentifier: "staticheadercell")
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                   reuseIdentifier: "staticheadercell")
        }
        cell?.detailTextLabel?.text = self.userCurrentAddress 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
}

extension ISSPassTimesViewController: MKMapViewDelegate {
    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        let customPointAnnotation = annotation as! ISSCustomPointAnnotation
        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)
        
        return annotationView
    }
    
    @objc func showTurnOnLocationServiceAlert(_ notification: NSNotification){
        let alert = UIAlertController(title: "Turn on Location Service", message: "To use location tracking feature of the app, please turn on the location service from the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}


