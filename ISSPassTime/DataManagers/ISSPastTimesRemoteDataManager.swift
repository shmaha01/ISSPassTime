
//
//  ISSPastTimesRemoteDataManager.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//
import CoreLocation

class ISSPassTimesRemoteDataManager:ISSPassTimesRemoteDataManagerInputProtocol {

    func retrieveISSCurrentLocation() {
        ISSService().getISSLocation {
            [unowned self] (outcome:Outcome) in
            switch outcome {
            case .Value(let data):
                let currentISSLocation = (data as! ISSNowDataModel)
                self.remoteRequestHandler?.onISSCurrentLocationDataRetrieved(currentISSLocation)
            case .Error(let e, _):
                print (e.debugDescription)
            }
        }
    }
    
    var remoteRequestHandler: ISSPassTimesRemoteDataManagerOutputProtocol?
    
    func retrieveISSPassTimesList() {
            var currentUserAddress = ""
            LocationService.sharedInstance.startUpdatingLocation { (outcome:Outcome) in
                switch outcome{
                case .Value(let currentLocation):
                    // get pass times for the current location
                    ISSService().getPassTime(forLocation: currentLocation as! CLLocation) { (outcome) in
                        switch outcome {
                        case .Value(let data):
                                let passTimes = data as! ISSPassTimeDataModel
                                LocationService.sharedInstance.getReverseGeocodeLocation(location: currentLocation as! CLLocation, completion: { (outcome) -> Void in
                                    switch outcome {
                                    case .Value(let data):
                                        currentUserAddress = data as! String
                                         self.remoteRequestHandler?.onPassTimesRetrieved(passTimes, currentAddress: currentUserAddress)
                                    case .Error(let e, _):
                                        print (e.debugDescription)
                                    }
                                })
                                self.remoteRequestHandler?.onPassTimesRetrieved(passTimes, currentAddress: currentUserAddress)
                        case .Error(let e, _):
                            print (e.debugDescription)
                        self.remoteRequestHandler?.onPassTimesRetrieved(ISSPassTimesLocalDataManager().retrieveISSPassTimesList(),currentAddress: "Could not Reverse Geocode your location")
                            self.remoteRequestHandler?.onError(error: e)
                        }
                    }
                case .Error(let e, _):
                    print(e.description)
                     self.remoteRequestHandler?.onError(error: e)
                }
            }
    }
    
}
