//
//  ISSPassTimesLocalDataManager.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import CoreData

class ISSPassTimesLocalDataManager:ISSPassTimesLocalDataManagerInputProtocol {
    
    func retrieveISSPassTimesList() -> ISSPassTimeDataModel{
        return self.loadJson(filename: "iss-pass")!
    }
    
    func loadJson(filename fileName: String) -> ISSPassTimeDataModel? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(ISSPassTimeDataModel.self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }

}
