//
//  ISSPassTimesWireFrame.swift
//  ISSPassTime
//
//  Created by M, Shwetha on 19/02/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation

import UIKit

class ISSPassTimesWireFrame: ISSPassTimesWireFrameProtocol {
    class func createISSPassTimesModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "ISSPassTimesNavigationController")
        if let view = navController.childViewControllers.first as? ISSPassTimesViewController {
            let presenter: ISSPassTimesPresenterProtocol & ISSPassTimesInteractorOutputProtocol = ISSPassTimesPresenter()
            let interactor: ISSPassTimesInteractorInputProtocol & ISSPassTimesRemoteDataManagerOutputProtocol = ISSPassTimesInteractor()
            let localDataManager: ISSPassTimesLocalDataManagerInputProtocol = ISSPassTimesLocalDataManager()
            let remoteDataManager: ISSPassTimesRemoteDataManagerInputProtocol = ISSPassTimesRemoteDataManager()
            let wireFrame: ISSPassTimesWireFrameProtocol = ISSPassTimesWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }

    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "ISSPassTimes", bundle: Bundle.main)
    }
    
}
